package br.com.acesso.acesso.models.DTOs;

import java.time.LocalDateTime;

public class EnvioAcessoKafka {

    private int idAcesso;
    private int idPorta;
    private int idCliente;
    private LocalDateTime dataHoraAcesso;

    public EnvioAcessoKafka() {
    }

    public EnvioAcessoKafka(int idAcesso, int idPorta, int idCliente, LocalDateTime dataHoraAcesso) {
        this.idAcesso = idAcesso;
        this.idPorta = idPorta;
        this.idCliente = idCliente;
        this.dataHoraAcesso = dataHoraAcesso;
    }

    public int getIdAcesso() {
        return idAcesso;
    }

    public void setIdAcesso(int idAcesso) {
        this.idAcesso = idAcesso;
    }

    public int getIdPorta() {
        return idPorta;
    }

    public void setIdPorta(int idPorta) {
        this.idPorta = idPorta;
    }

    public int getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }

    public LocalDateTime getDataHoraAcesso() {
        return dataHoraAcesso;
    }

    public void setDataHoraAcesso(LocalDateTime dataHoraAcesso) {
        this.dataHoraAcesso = dataHoraAcesso;
    }

    @Override
    public String toString() {
        return "EnvioAcessoKafka{" +
                "idAcesso=" + idAcesso +
                ", idPorta=" + idPorta +
                ", idCliente=" + idCliente +
                ", dataHoraAcesso=" + dataHoraAcesso +
                '}';
    }
}
