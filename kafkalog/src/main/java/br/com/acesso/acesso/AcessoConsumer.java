package br.com.acesso.acesso;

import java.io.FileWriter;
import java.io.IOException;

import br.com.acesso.acesso.models.DTOs.EnvioAcessoKafka;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
public class AcessoConsumer {

    @KafkaListener(topics = "spec3-alan-nigri-1", groupId = "Alan")
    public void receber(@Payload EnvioAcessoKafka envioAcessoKafka) {
        generateCsvFile("/home/a2w/workspace-espec/Acesso-MS/logKafka.csv", envioAcessoKafka);
        System.out.println("recebido!!");
    }

    private static void generateCsvFile(String sFileName, EnvioAcessoKafka envioAcessoKafka) {
        try {
            FileWriter writer = new FileWriter(sFileName);

            writer.append(envioAcessoKafka.toString());

            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}