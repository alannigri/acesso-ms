package br.com.acesso.porta.services;

import br.com.acesso.porta.exceptions.PortaNaoEncontradaException;
import br.com.acesso.porta.models.Porta;
import br.com.acesso.porta.repositories.PortaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PortaService {

    @Autowired
    private PortaRepository portaRepository;

    public Porta criarPorta (Porta porta){
        return portaRepository.save(porta);
    }

    public Porta consultarPorta (int idPorta){
        Optional<Porta> portaOptional = portaRepository.findById(idPorta);
        if(!portaOptional.isPresent()){
            throw new PortaNaoEncontradaException();
        }
        return portaOptional.get();
    }
}
