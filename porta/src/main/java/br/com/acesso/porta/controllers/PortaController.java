package br.com.acesso.porta.controllers;

import br.com.acesso.porta.models.Porta;
import br.com.acesso.porta.services.PortaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/porta")
public class PortaController {

    @Autowired
    private PortaService portaService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Porta criarPorta(@RequestBody @Valid Porta porta) {
        porta = portaService.criarPorta(porta);
        return porta;
    }

    @GetMapping("/{idPorta}")
    public Porta consultarPorta(@PathVariable int idPorta) {
        Porta porta = portaService.consultarPorta(idPorta);
        return porta;
    }
}
