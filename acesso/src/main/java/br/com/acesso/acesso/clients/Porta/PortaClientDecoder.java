package br.com.acesso.acesso.clients.Porta;

import br.com.acesso.acesso.exceptions.ClienteNaoEncontradoException;
import br.com.acesso.acesso.exceptions.PortaNaoEncontradaException;
import feign.Response;
import feign.codec.ErrorDecoder;

public class PortaClientDecoder implements ErrorDecoder {

    private ErrorDecoder errorDecoder = new ErrorDecoder.Default();

    @Override
    public Exception decode(String s, Response response) {
        if (response.status() == 404) {
            throw new PortaNaoEncontradaException();
        }else{
            return errorDecoder.decode(s, response);
        }

    }
}
