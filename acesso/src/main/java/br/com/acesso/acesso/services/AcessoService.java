package br.com.acesso.acesso.services;

import br.com.acesso.acesso.clients.Cliente.ClienteClient;
import br.com.acesso.acesso.clients.Porta.PortaClient;
import br.com.acesso.acesso.exceptions.AcessoNaoEncontradoException;
import br.com.acesso.acesso.models.Acesso;
import br.com.acesso.acesso.models.DTOs.EnvioAcessoKafka;
import br.com.acesso.acesso.repositories.AcessoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Optional;

@Service
public class AcessoService {

    @Autowired
    private AcessoRepository acessoRepository;

    @Autowired
    private ClienteClient clienteClient;

    @Autowired
    private PortaClient portaClient;

    @Autowired
    private AcessoProducer acessoProducer;

    public Acesso criarAcesso(Acesso acesso) {
        clienteClient.consultarCliente(acesso.getIdCliente());
        portaClient.consultarPorta(acesso.getIdPorta());
        try {
            consultarAcesso(acesso);
        } catch (RuntimeException e) {
            acessoRepository.save(acesso);
        }
        return acesso;
    }

    public Acesso consultarAcesso(Acesso acesso) {
        Optional<Acesso> acessoOptional = acessoRepository.findByIdClienteAndIdPorta(acesso.getIdCliente(),
                acesso.getIdPorta());
        if (!acessoOptional.isPresent()) {
            throw new AcessoNaoEncontradoException();
        }
        acessoProducer.EnviarAoKafka(acessoOptional.get());
        return acessoOptional.get();
    }

    public void deletarAcesso(Acesso acesso) {
        acesso = consultarAcesso(acesso);
        acessoRepository.deleteById(acesso.getIdAcesso());
    }
}
