package br.com.acesso.acesso.controllers;

import br.com.acesso.acesso.models.Acesso;
import br.com.acesso.acesso.services.AcessoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.ws.rs.Path;

@RestController
@RequestMapping("/acesso")
public class AcessoController {

    @Autowired
    private AcessoService acessoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Acesso criarAcesso(@RequestBody @Valid Acesso acesso) {
        return acessoService.criarAcesso(acesso);
    }

    @GetMapping("/{cliente_id}/{porta_id}")
    public Acesso consultarAcesso(@PathVariable int cliente_id, @PathVariable int porta_id) {
        Acesso acesso = new Acesso();
        acesso.setIdCliente(cliente_id);
        acesso.setIdPorta(porta_id);
        acessoService.consultarAcesso(acesso);
        return acesso;
    }

    @DeleteMapping("/{cliente_id}/{porta_id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deletarAcesso(@PathVariable int cliente_id, @PathVariable int porta_id) {
        Acesso acesso = new Acesso();
        acesso.setIdCliente(cliente_id);
        acesso.setIdPorta(porta_id);
        acessoService.deletarAcesso(acesso);
    }
}
