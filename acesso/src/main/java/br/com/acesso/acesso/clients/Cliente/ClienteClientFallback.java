package br.com.acesso.acesso.clients.Cliente;

import br.com.acesso.acesso.models.DTOs.Cliente;

public class ClienteClientFallback implements  ClienteClient{

    @Override
    public Cliente consultarCliente(int idCliente) {
        throw new ClienteClientException();
    }
}
