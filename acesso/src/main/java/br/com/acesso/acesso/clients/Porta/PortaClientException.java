package br.com.acesso.acesso.clients.Porta;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_GATEWAY, reason = "O sistema de portas se encontra offline")
public class PortaClientException extends RuntimeException{
}
