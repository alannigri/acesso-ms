package br.com.acesso.acesso.clients.Porta;

import br.com.acesso.acesso.models.DTOs.Porta;

public class PortaClientFallback implements PortaClient {

    @Override
    public Porta consultarPorta(int idPorta) {
        throw new PortaClientException();
    }
}

