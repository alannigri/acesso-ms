package br.com.acesso.acesso.models.DTOs;

import javax.validation.constraints.NotNull;

public class Porta {

    int idPorta;
    String andar;
    String sala;

    public int getIdPorta() {
        return idPorta;
    }

    public void setIdPorta(int idPorta) {
        this.idPorta = idPorta;
    }

    public String getAndar() {
        return andar;
    }

    public void setAndar(String andar) {
        this.andar = andar;
    }

    public String getSala() {
        return sala;
    }

    public void setSala(String sala) {
        this.sala = sala;
    }

}
