package br.com.acesso.acesso.clients.Cliente;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_GATEWAY, reason = "O sistema de clientes se encontra offline")
public class ClienteClientException extends RuntimeException {
}
