package br.com.acesso.acesso.services;

import br.com.acesso.acesso.models.Acesso;
import br.com.acesso.acesso.models.DTOs.EnvioAcessoKafka;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class AcessoProducer {

    @Autowired
    private KafkaTemplate<String, EnvioAcessoKafka> acessoKafka;

    public void EnviarAoKafka(Acesso acesso) {
        EnvioAcessoKafka envioAcessoKafka = new EnvioAcessoKafka(acesso.getIdAcesso(), acesso.getIdPorta(),
                acesso.getIdCliente(), LocalDateTime.now());
        acessoKafka.send("spec3-alan-nigri-1", envioAcessoKafka);
    }
}
