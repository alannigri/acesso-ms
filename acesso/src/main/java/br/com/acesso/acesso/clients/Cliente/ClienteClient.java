package br.com.acesso.acesso.clients.Cliente;

import br.com.acesso.acesso.models.DTOs.Cliente;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "cliente-acesso", configuration = ClienteClientConfiguration.class)
public interface ClienteClient {

    @GetMapping("/cliente/{idCliente}")
    Cliente consultarCliente (@PathVariable int idCliente);
}