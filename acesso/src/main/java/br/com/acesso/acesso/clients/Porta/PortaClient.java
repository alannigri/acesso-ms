package br.com.acesso.acesso.clients.Porta;

import br.com.acesso.acesso.models.DTOs.Cliente;
import br.com.acesso.acesso.models.DTOs.Porta;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "porta-acesso")
public interface PortaClient {

    @GetMapping("/porta/{idPorta}")
    Porta consultarPorta (@PathVariable int idPorta);
}


