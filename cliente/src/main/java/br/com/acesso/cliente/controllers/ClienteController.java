package br.com.acesso.cliente.controllers;

import br.com.acesso.cliente.models.Cliente;
import br.com.acesso.cliente.services.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/cliente")
public class ClienteController {

    @Autowired
    private ClienteService clienteService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Cliente criarCliente (@RequestBody @Valid Cliente cliente){
        cliente = clienteService.criarCliente(cliente);
        return cliente;
    }

    @GetMapping("{idCliente}")
    public Cliente consultarCliente(@PathVariable int idCliente){
        Cliente cliente = clienteService.consultarCliente(idCliente);
        return cliente;
    }
}

