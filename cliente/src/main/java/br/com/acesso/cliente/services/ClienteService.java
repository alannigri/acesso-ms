package br.com.acesso.cliente.services;

import br.com.acesso.cliente.exceptions.ClienteNaoEncontradoException;
import br.com.acesso.cliente.models.Cliente;
import br.com.acesso.cliente.repositories.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ClienteService {

    @Autowired
    private ClienteRepository clienteRepository;


    public Cliente criarCliente (Cliente cliente){
        return clienteRepository.save(cliente);
    }

    public Cliente consultarCliente (int idCliente){
        Optional<Cliente> clienteOptional = clienteRepository.findById(idCliente);
        if(!clienteOptional.isPresent()){
            throw new ClienteNaoEncontradoException();
        }
        return clienteOptional.get();
    }
}
